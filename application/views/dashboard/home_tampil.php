<!DOCTYPE html>
<html>
<head>
  <title>Data Peminjaman</title>
  <link rel="stylesheet" type="text/css" href="<?php echo base_url('mod/bootstrap/css/bootstrap.min.css'); ?>">
  <script type="text/javascript" src="<?php echo base_url('mod/jquery/jquery-3.3.1.min.js'); ?>"></script>
  <script type="text/javascript" src="<?php echo base_url('mod/bootstrap/js/bootstrap.min.js'); ?>"></script>

</head>
<body>
    <a href="<?=base_url()?>index.php">Back</a>

<center>    
 <body style="margin: 20px;">
  <div class="panel panel-primary">
    <div class="panel-heading">
      <b class="col-md-10">Data Peminjaman</b>
      <button data-toggle="modal" data-target="#addModal" class="btn btn-success">Tambah Data +</button>
    </div>
    <div class="panel-body">
      <div class="table-responsive">
        <table class="table table-striped table-bordered table-hover">
          <thead>
            <tr>
              <th>No</th>
              <th>ID</th>
              <th>Nama</th>
              <th>NIS</th>
              <th>Judul</th>
              <th>Kode</th>
              <th>Tanggal Pinjam</th>
              <th>Status</th>
              <th>Option</th>
            </tr>
          </thead>
          <tbody id="tbl_data">
            
          </tbody>
        </table>
      </div>
    </div>
  </div>
</body>


        <!-- Modal Tambah-->
  <div id="addModal" class="modal fade" role="dialog">
    <div class="modal-dialog">

      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Tambah Data</h4>
        </div>
        <div class="modal-body">
          <form>
            <div class="form-group">
              <label for="nama" class="col-sm-1">Nama</label>
              <input type="text" name="nama" class="form-control"></input>
            </div>
            <div class="form-group">
              <label for="nis" class="col-sm-1">NIS</label>
              <input type="text" name="nis" class="form-control"></input>
            </div>
            <div class="form-group">
              <label for="judul">Judul</label>
              <select name="id_pinjam" id="id_pinjam" class="form-control">
                <option value="">PILIH BUKU</option>
                <?php foreach ($buku_views as $row):?> 
                  <option value="<?php echo $row->id_buku:?>"><?php echo $row->nama_buku;?></option>
                  <?php endforeach;?>
              </select>
            </div>
            <div class="form-group">
              <label for="kode" class="col-sm-1">Kode</label>
              <input type="text" name="kode" class="form-control"></input>
            </div>
            <div class="form-group">
              <label for="tgl_pinjam" class="col-sm-3">Tgl Pinjam</label>
              <input type="date" name="tgl_pinjam" class="form-control"></input>
            </div>
            <div class='form-group'>
                <label>Status</label>
                <select class='form-control' id='status' name='status'>
                    <option value="">--pilih--</option>
                    <option value="Tersedia">Tersedia</option>
                    <option value="Dipinjam">Dipinjam</option>
                    <option value="Selesai">Selesai</option>
                </select>
            </div>

          </form>
        </div>
        <div class="modal-footer">
         <button type="button" class="btn btn-success" id="btn_add_data">Simpan</button>
         <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>

    </div>
  </div>

  <!-- Modal Edit-->
  <div id="editModal" class="modal fade" role="dialog">
    <div class="modal-dialog">

      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Edit Data</h4>
        </div>
        <div class="modal-body">
          <form>
            <div class="form-group">
              <label for="id_pinjam" class="col-sm-1">ID</label>
              <input type="text" name="id_edit" class="form-control"></input>
            </div>
            <div class="form-group">
              <label for="nama" class="col-sm-1">Nama</label>
              <input type="text" name="nama_edit" class="form-control"></input>
            </div>
            <div class="form-group">
              <label for="nis" class="col-sm-1">NIS</label>
              <input type="text" name="nis_edit" class="form-control"></input>
            </div>
            <div class="form-group">
              <label for="judul" class="col-sm-1">Judul</label>
              <input type="text" name="judul_edit" class="form-control"></input>
            </div>
            <div class="form-group">
              <label for="kode" class="col-sm-1">Kode</label>
              <input type="text" name="kode_edit" class="form-control"></input>
            </div>
            <div class="form-group">
              <label for="tgl_pinjam" class="col-sm-3">Tanggal Pinjam</label>
              <input type="date" name="tgl_edit" class="form-control"></input>
            </div>
            <div class="form-group">
                <label for="status" class="col-sm-1">Status</label>
                <select class='form-control' id='status' name='status_edit'>
                    <option value="">--pilih--</option>
                    <option value="Tersedia">Tersedia</option>
                    <option value="Dipinjam">Dipinjam</option>
                    <option value="Selesai">Selesai</option>
                </select>
            </div>

          </form>
        </div>
        <div class="modal-footer">
         <button type="submit" class="btn btn-success" id="btn_update_data">Update</button>
         <button type="submit" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>

    </div>
  </div>

</html>
<script type="text/javascript">
  $(document).ready(function(){
    tampil_data();
    //Menampilkan Data di tabel
    function tampil_data(){
      $.ajax({
        url: '<?php echo site_url('Home/ambilData'); ?>',
        type: 'POST',
        dataType: 'json',
        success: function(response){
          var i;
          var no = 0;
          var html = "";
          for(i=0;i < response.length ; i++){
            no++;
            html = html + '<tr>'
                  + '<td>' + no  + '</td>'
                  + '<td>' + response[i].id_pinjam  + '</td>'
                  + '<td>' + response[i].nama  + '</td>'
                  + '<td>' + response[i].nis  + '</td>'
                  + '<td>' + response[i].judul  + '</td>'
                  + '<td>' + response[i].kode  + '</td>'
                  + '<td>' + response[i].tgl_pinjam  + '</td>'
                  + '<td>' + response[i].status  + '</td>'
                  + '<td style="width: 16.66%;">' + '<span><button data-id="'+response[i].id_pinjam+'" class="btn btn-primary btn_edit">Edit</button><button style="margin-left: 5px;" data-id="'+response[i].id_pinjam+'" class="btn btn-danger btn_hapus">Hapus</button></span>'  + '</td>'
                  + '</tr>';
          }
          $("#tbl_data").html(html);
        }

      });
    }
    //Hapus Data dengan konfirmasi
    $("#tbl_data").on('click','.btn_hapus',function(){
      var id_pinjam = $(this).attr('data-id');
      var status = confirm('Yakin ingin menghapus?');
      if(status){
        $.ajax({
          url: '<?php echo site_url('Home/hapusData'); ?>',
          type: 'POST',
          data: {id_pinjam:id_pinjam},
          success: function(response){
            tampil_data();
          }
        })
      }
    })
    //Menambahkan Data ke database
    $("#btn_add_data").on('click',function(){
      var id_pinjam = $('input[name="id_pinjam"]').val();
      var nama = $('input[name="nama"]').val();
      var nis = $('input[name="nis"]').val();
      var judul = $('select[name="judul"]').val();
      var kode = $('input[name="kode"]').val();
      var tgl_pinjam = $('input[name="tgl_pinjam"]').val();
      var status = $('select[name="status"]').val();
      $.ajax({
        url: '<?php echo site_url('Home/tambahData'); ?>',
        type: 'POST',
        data: {nama:nama,nis:nis,id_pinjam:id_pinjam,judul:judul,kode:kode,tgl_pinjam:tgl_pinjam,status:status},
        success: function(response){
          $('input[name="id_pinjam"]').val("");
          $('input[name="nama"]').val("");
          $('input[name="nis"]').val("");
          $('input[select="judul"]').val("");
          $('input[name="kode"]').val("");
          $('input[name="tgl_pinjam"]').val("");
          $('input[select="status"]').val("");
          $("#addModal").modal('hide');
          tampil_data();
        }
      })

    });
    //Memunculkan modal edit
    $("#tbl_data").on('click','.btn_edit',function(){
      var id_pinjam = $(this).attr('data-id');
      $.ajax({
        url: '<?php echo site_url('Home/ambilDataByNoinduk'); ?>',
        type: 'POST',
        data: {id_pinjam:id_pinjam},
        dataType: 'json',
        success: function(response){
          console.log(response);
          $("#editModal").modal('show');
          $('input[name="id_edit"]').val(response[0].id_pinjam);
          $('input[name="nama_edit"]').val(response[0].nama);
          $('input[name="nis_edit"]').val(response[0].nis);
          $('input[name="judul_edit"]').val(response[0].judul);
          $('input[name="kode_edit"]').val(response[0].kode);
          $('input[name="tgl_edit"]').val(response[0].tgl_pinjam);
          $('input[select="status_edit"]').val(response[0].status);

        }
      })
    });

    //Meng-Update Data
    $("#btn_update_data").on('click',function(){
      var id_pinjam = $('input[name="id_edit"]').val();
      var nama = $('input[name="nama_edit"]').val();
      var nis = $('input[name="nis_edit"]').val();
      var judul = $('input[name="judul_edit"]').val();
      var kode = $('input[name="kode_edit"]').val();
      var tgl_pinjam = $('input[name="tgl_edit"]').val();
      var status = $('select[name="status_edit"]').val();
      $.ajax({
        url: '<?php echo site_url('Home/perbaruiData'); ?>',
        type: 'POST',
        data: {nama:nama,nis:nis,id_pinjam:id_pinjam,judul:judul,kode:kode,tgl_pinjam:tgl_pinjam,status:status},
        success: function(response){
          $('input[name="id_edit"]').val("");
          $('input[name="nama_edit"]').val("");
          $('input[name="nis_edit"]').val("");
          $('input[name="judul_edit"]').val("");
          $('input[name="kode_edit"]').val("");
          $('input[name="tgl_edit"]').val("");
          $('input[select="status_edit"]').val("");
          $("#editModal").modal('hide');
          tampil_data();                                                  
        }
      })

    });
  });
</script>