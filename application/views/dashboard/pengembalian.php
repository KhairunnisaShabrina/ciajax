<!DOCTYPE html>
<html>
<head>
  <title>Data Pengembalian</title>
  <link rel="stylesheet" type="text/css" href="<?php echo base_url('mod/bootstrap/css/bootstrap.min.css'); ?>">
  <script type="text/javascript" src="<?php echo base_url('mod/jquery/jquery-3.3.1.min.js'); ?>"></script>
  <script type="text/javascript" src="<?php echo base_url('mod/bootstrap/js/bootstrap.min.js'); ?>"></script>

</head>
<body>
    <a href="<?=base_url()?>index.php">Back</a>

<center>    
 <body style="margin: 20px;">
  <div class="panel panel-primary">
    <div class="panel-heading">
      <b class="col-md-10">Data Pengembalian</b>
      <button data-toggle="modal" data-target="#addModal" class="btn btn-success">Input +</button>
    </div>
    <div class="panel-body">
      <div class="table-responsive">
        <table class="table table-striped table-bordered table-hover">
          <thead>
            <tr>
              <th>No</th>
              <th>ID</th>
              <th>Nama</th>
              <th>NIS</th>
              <th>Kode</th>
              <th>Judul</th>
              <th>Tanggal Pinjam</th>
              <th>Tanggal Kembali</th>
              <th>Option</th>
            </tr>
          </thead>
          <tbody id="tbl_data">
            
          </tbody>
        </table>
      </div>
    </div>
  </div>
</body>


        <!-- Modal Tambah-->
  <div id="addModal" class="modal fade" role="dialog">
    <div class="modal-dialog">

      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Masukan Data</h4>
        </div>
        <div class="modal-body">
          <form>
            <div class="form-group">
              <label for="nama" class="col-sm-1">Nama</label>
              <input type="text" name="nama" class="form-control"></input>
            </div>
            <div class="form-group">
              <label for="nis" class="col-sm-1">NIS</label>
              <input type="text" name="nis" class="form-control"></input>
            </div>
            <div class="form-group">
              <label for="kode" class="col-sm-1">Kode</label>
              <input type="text" name="kode" class="form-control"></input>
            </div>
            <div class="form-group">
              <label for="judul" class="col-sm-1">Judul</label>
              <input type="text" name="judul" class="form-control"></input>
            </div>
            <div class="form-group">
              <label for="tgl_pinjam" class="col-sm-3">Tgl Pinjam</label>
              <input type="date" name="tgl_pinjam" class="form-control"></input>
            </div>
            <div class="form-group">
              <label for="tgl_kembali" class="col-sm-3">Tgl Kembali</label>
              <input type="date" name="tgl_kembali" class="form-control"></input>
            </div>

          </form>
        </div>
        <div class="modal-footer">
         <button type="submit" class="btn btn-success" id="btn_add_data">Simpan</button>
         <button type="submit" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>

    </div>
  </div>

  <!-- Modal Edit-->
  <div id="editModal" class="modal fade" role="dialog">
    <div class="modal-dialog">

      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Edit Data</h4>
        </div>
        <div class="modal-body">
          <form>
            <div class="form-group">
              <label for="id_kembali" class="col-sm-1">ID</label>
              <input type="text" name="id_edit" class="form-control"></input>
            </div>
            <div class="form-group">
              <label for="nama" class="col-sm-1">Nama</label>
              <input type="text" name="nama_edit" class="form-control"></input>
            </div>
            <div class="form-group">
              <label for="nis" class="col-sm-1">NIS</label>
              <input type="text" name="nis_edit" class="form-control"></input>
            </div>
            <div class="form-group">
              <label for="kode" class="col-sm-1">Kode</label>
              <input type="text" name="kode_edit" class="form-control"></input>
            </div>
            <div class="form-group">
              <label for="judul" class="col-sm-1">Judul</label>
              <input type="text" name="judul_edit" class="form-control"></input>
            </div>
            <div class="form-group" >
              <label for="tgl_pinjam" class="col-sm-1">Tgl Pinjam</label>
              <input type="date" name="tgl_edit" class="form-control"></input>
            </div>
            <div class="form-group">
              <label for="tgl_kembali" class="col-sm-1">Tgl Kembali</label>
              <input type="date" name="tgl_edit" class="form-control"></input>
            </div>

          </form>
        </div>
        <div class="modal-footer">
         <button type="submit" class="btn btn-success" id="btn_update_data">Update</button>
         <button type="submit" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>

    </div>
  </div>

</html>
<script type="text/javascript">
  $(document).ready(function(){
    tampil_data();
    //Menampilkan Data di tabel
    function tampil_data(){
      $.ajax({
        url: '<?php echo site_url('Pengembalian/ambilData'); ?>',
        type: 'POST',
        dataType: 'json',
        success: function(response){
          var i;
          var no = 0;
          var html = "";
          for(i=0;i < response.length ; i++){
            no++;
            html = html + '<tr>'
                  + '<td>' + no  + '</td>'
                  + '<td>' + response[i].id_kembali + '</td>'
                  + '<td>' + response[i].nama  + '</td>'
                  + '<td>' + response[i].nis  + '</td>'
                  + '<td>' + response[i].kode  + '</td>'
                  + '<td>' + response[i].judul  + '</td>'
                  + '<td>' + response[i].tgl_pinjam  + '</td>'
                  + '<td>' + response[i].tgl_kembali  + '</td>'
                  + '<td style="width: 16.66%;">' + '<span><button data-id="'+response[i].id_kembali+'" class="btn btn-primary btn_edit">Edit</button><button style="margin-left: 5px;" data-id="'+response[i].id_kembali+'" class="btn btn-danger btn_hapus">Hapus</button></span>'  + '</td>'
                  + '</tr>';
          }
          $("#tbl_data").html(html);
        }

      });
    }
    //Hapus Data dengan konfirmasi
    $("#tbl_data").on('click','.btn_hapus',function(){
      var id_kembali = $(this).attr('data-id');
      var status = confirm('Yakin ingin menghapus?');
      if(status){
        $.ajax({
          url: '<?php echo site_url('Pengembalian/hapusData'); ?>',
          type: 'POST',
          data: {id_kembali:id_kembali},
          success: function(response){
            tampil_data();
          }
        })
      }
    })
    //Menambahkan Data ke database
    $("#btn_add_data").on('click',function(){
      var id_kembali = $('input[name="id_kembali"]').val();
      var nama = $('input[name="nama"]').val();
      var nis = $('input[name="nis"]').val();
      var kode = $('input[name="kode"]').val();
      var judul = $('input[name="judul"]').val();
      var tgl_pinjam = $('input[name="tgl_pinjam"]').val();
      var tgl_kembali = $('input[name="tgl_kembali"]').val();
      $.ajax({
        url: '<?php echo site_url('Pengembalian/tambahData'); ?>',
        type: 'POST',
        data: {nama:nama,nis:nis,id_kembali:id_kembali,judul:judul,kode:kode,tgl_pinjam:tgl_pinjam,tgl_kembali:tgl_kembali},
        success: function(response){
          $('input[name="id_kembali"]').val("");
          $('input[name="nama"]').val("");
          $('input[name="nis"]').val("");
          $('input[name="judul"]').val("");
          $('input[name="kode"]').val("");
          $('input[name="tgl_pinjam"]').val("");
          $('input[name="tgl_kembali"]').val("");
          $("#addModal").modal('hide');
          tampil_data();
        }
      })

    });
    //Memunculkan modal edit
    $("#tbl_data").on('click','.btn_edit',function(){
      var id_kembali = $(this).attr('data-id');
      $.ajax({
        url: '<?php echo site_url('Pengembalian/ambilDataByNoinduk'); ?>',
        type: 'POST',
        data: {id_kembali:id_kembali},
        dataType: 'json',
        success: function(response){
          console.log(response);
          $("#editModal").modal('show');
          $('input[name="id_edit"]').val(response[0].id_edit);
          $('input[name="nama_edit"]').val(response[0].nama);
          $('input[name="nis_edit"]').val(response[0].nis);
          $('input[name="judul_edit"]').val(response[0].judul);
          $('input[name="kode_edit"]').val(response[0].kode);
          $('input[name="tgl_edit"]').val(response[0].tgl_pinjam);
          $('input[name="tgl_edit"]').val(response[0].tgl_kembali);

        }
      })
    });

    //Meng-Update Data
    $("#btn_update_data").on('click',function(){
      var id_kembali = $('input[name="id_edit"]').val();
      var nama = $('input[name="nama_edit"]').val();
      var nis = $('input[name="nis_edit"]').val();
      var judul = $('input[name="judul_edit"]').val();
      var kode = $('input[name="kode_edit"]').val();
      var tgl_pinjam = $('input[name="tgl_edit"]').val();
      var tgl_kembali = $('input[name="tgl_edit"]').val();
      $.ajax({
        url: '<?php echo site_url('Pengembalian/perbaruiData'); ?>',
        type: 'POST',
        data: {nama:nama,nis:nis,id_kembali:id_kembali,judul:judul,kode:kode,tgl_pinjam:tgl_pinjam,tgl_kembali:tgl_kembali},
        success: function(response){
          $('input[name="id_edit"]').val("");
          $('input[name="nama_edit"]').val("");
          $('input[name="nis_edit"]').val("");
          $('input[name="judul_edit"]').val("");
          $('input[name="kode_edit"]').val("");
          $('input[name="tgl_edit"]').val("");
          $('input[name="tgl_edit"]').val("");
          $("#editModal").modal('hide');
          tampil_data();                                                  
        }
      })

    });
  });
</script>
