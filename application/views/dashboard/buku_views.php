<!DOCTYPE html>
<html>
<head>
  <title>Data Buku</title>
  <link rel="stylesheet" type="text/css" href="<?php echo base_url('mod/bootstrap/css/bootstrap.min.css'); ?>">
  <script type="text/javascript" src="<?php echo base_url('mod/jquery/jquery-3.3.1.min.js'); ?>"></script>
  <script type="text/javascript" src="<?php echo base_url('mod/bootstrap/js/bootstrap.min.js'); ?>"></script>

</head>
<body>
    <a href="<?=base_url()?>index.php">Back</a>

<center>    
 <body style="margin: 20px;">
  <div class="panel panel-primary">
    <div class="panel-heading">
      <b class="col-md-10">Data Buku</b>
      <center><button data-toggle="modal" data-target="#addModal" class="btn btn-success">Tambah Data +</button></center>
    </div>
    <div class="panel-body">
      <div class="table-responsive">
        <table class="table table-striped table-bordered table-hover">
          <thead>
            <tr>
              <th>No</th>
              <th>ID</th>
              <th>Nama buku</th>
              <th>Penerbit</th>
              <th>Pengarang</th>
              <th>Jumlah</th>
              <th>Jenis</th>
              <th>Option</th>
            </tr>
          </thead>
          <tbody id="tbl_data">
            
          </tbody>
        </table>
      </div>
    </div>
  </div>
</body>


        <!-- Modal Tambah-->
  <div id="addModal" class="modal fade" role="dialog">
    <div class="modal-dialog">

      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Tambah Data</h4>
        </div>
        <div class="modal-body">
          <form>
            <div class="form-group">
              <label for="nama_buku" class="col-sm-1">Nama buku</label>
              <input type="text" name="nama_buku" class="form-control"></input>
            </div>
            <div class="form-group">
              <label for="penerbit" class="col-sm-1">Penerbit</label>
              <input type="text" name="penerbit" class="form-control"></input>
            </div>
            <div class="form-group">
              <label for="pengarang" class="col-sm-1">Pengarang</label>
              <input type="text" name="pengarang" class="form-control"></input>
            </div>
            <div class="form-group">
              <label for="jumlah" class="col-sm-1">Jumlah</label>
              <input type="text" name="jumlah" class="form-control"></input>
            </div>
            <div class="form-group">
              <label for="jenis" class="col-sm-1">Jenis</label>
              <input type="text" name="jenis" class="form-control"></input>
            </div>

          </form>
        </div>
        <div class="modal-footer">
         <button type="submit" class="btn btn-success" id="btn_add_data">Simpan</button>
         <button type="submit" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>

    </div>
  </div>

  <!-- Modal Edit-->
  <div id="editModal" class="modal fade" role="dialog">
    <div class="modal-dialog">

      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="submit" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Edit Data</h4>
        </div>
        <div class="modal-body">
          <form>
            <div class="form-group">
              <label for="nama_buku">ID buku</label>
              <input type="text" name="id_buku" class="form-control"></input>
            </div>
            <div class="form-group">
              <label for="nama_buku">Nama buku</label>
              <input type="text" name="nama_edit" class="form-control"></input>
            </div>
            <div class="form-group">
              <label for="penerbit">Penerbit</label>
              <input type="text" name="penerbit_edit" class="form-control"></input>
            </div>
            <div class="form-group">
              <label for="pengarang">Pengarang</label>
              <input type="text" name="pengarang_edit" class="form-control"></input>
            </div>
             <div class="form-group">
              <label for="jumlah">Jumlah</label>
              <input type="text" name="jumlah_edit" class="form-control"></input>
            </div>
             <div class="form-group">
              <label for="jenis">Jenis</label>
              <input type="text" name="jenis_edit" class="form-control"></input>
            </div>

          </form>
        </div>
        <div class="modal-footer">
         <button type="submit" class="btn btn-success" id="btn_update_data">Update</button>
         <button type="submit" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>

    </div>
  </div>

</html>
<script type="text/javascript">
  $(document).ready(function(){
    tampil_data();
    //Menampilkan Data di tabel
    function tampil_data(){
      $.ajax({
        url: '<?php echo site_url('Buku_controllers/ambilData'); ?>',
        type: 'POST',
        dataType: 'json',
        success: function(response){
          var i;
          var no = 0;
          var html = "";
          for(i=0;i < response.length ; i++){
            no++;
            html = html + '<tr>'
                  + '<td>' + no  + '</td>'
                  + '<td>' + response[i].id_buku  + '</td>'
                  + '<td>' + response[i].nama_buku  + '</td>'
                  + '<td>' + response[i].penerbit  + '</td>'
                  + '<td>' + response[i].pengarang  + '</td>'
                  + '<td>' + response[i].jumlah  + '</td>'
                  + '<td>' + response[i].jenis + '</td>'
                  + '<td style="width: 16.66%;">' + '<span><button data-id="'+response[i].id_buku+'" class="btn btn-primary btn_edit">Edit</button><button style="margin-left: 5px;" data-id="'+response[i].id_buku+'" class="btn btn-danger btn_hapus">Hapus</button></span>'  + '</td>'
                  + '</tr>';
          }
          $("#tbl_data").html(html);
        }

      });
    }
    //Hapus Data dengan konfirmasi
    $("#tbl_data").on('click','.btn_hapus',function(){
      var id_buku = $(this).attr('data-id');
      var status = confirm('Yakin ingin menghapus?');
      if(status){
        $.ajax({
          url: '<?php echo site_url('Buku_controllers/hapusData'); ?>',
          type: 'POST',
          data: {id_buku:id_buku},
          success: function(response){
            tampil_data();
          }
        })
      }
    })
    //Menambahkan Data ke database
    $("#btn_add_data").on('click',function(){
      var id_buku = $('input[name="id_buku"]').val();
      var nama_buku = $('input[name="nama_buku"]').val();
      var penerbit = $('input[name="penerbit"]').val();
      var pengarang = $('input[name="pengarang"]').val();
      var jumlah = $('input[name="jumlah"]').val();
      var jenis = $('input[name="jenis"]').val();
      $.ajax({
        url: '<?php echo site_url('Buku_controllers/tambahData'); ?>',
        type: 'POST',
        data: {id_buku:id_buku,nama_buku:nama_buku,penerbit:penerbit,pengarang:pengarang,jumlah:jumlah,jenis:jenis},
        success: function(response){
          $('input[name="id_buku"]').val("");
          $('input[name="nama_buku"]').val("");
          $('input[name="penerbit"]').val("");
          $('input[name="pengarang"]').val("");
          $('input[name="jumlah"]').val("");
          $('input[name="jenis"]').val("");
          $("#addModal").modal('hide');
          tampil_data();
        }
      })

    });
    //Memunculkan modal edit
    $("#tbl_data").on('click','.btn_edit',function(){
      var id_buku = $(this).attr('data-id');
      $.ajax({
        url: '<?php echo site_url('Buku_controllers/ambilDataByNoinduk'); ?>',
        type: 'POST',
        data: {id_buku:id_buku},
        dataType: 'json',
        success: function(response){
          console.log(response);
          $("#editModal").modal('show');
          $('input[name="id_buku"]').val(response[0].id_buku);
          $('input[name="nama_edit"]').val(response[0].nama_buku);
          $('input[name="penerbit_edit"]').val(response[0].penerbit);
          $('input[name="pengarang_edit"]').val(response[0].pengarang);
          $('input[name="jumlah_edit"]').val(response[0].jumlah);
          $('input[name="jenis_edit"]').val(response[0].jenis);

        }
      })
    });

    //Meng-Update Data
    $("#btn_update_data").on('click',function(){
      var id_buku = $('input[name="id_buku"]').val();
      var nama_buku = $('input[name="nama_edit"]').val();
      var penerbit  = $('input[name="penerbit_edit"]').val();
      var pengarang = $('input[name="pengarang_edit"]').val();
      var jumlah = $('input[name="jumlah_edit"]').val();
      var jenis = $('input[name="jenis_edit"]').val();
      $.ajax({
        url: '<?php echo site_url('Buku_controllers/perbaruiData'); ?>',
        type: 'POST',
        data: {id_buku:id_buku,nama_buku:nama_buku,penerbit:penerbit,pengarang:pengarang,jumlah:jumlah,jenis:jenis},
        success: function(response){
          $('input[name="id_buku"]').val("");
          $('input[name="nama_edit"]').val("");
          $('input[name="penerbit_edit"]').val("");
          $('input[name="pengarang_edit"]').val("");
          $('input[name="jumlah_edit"]').val("");
          $('input[name="jenis_edit"]').val("");
          $("#editModal").modal('hide');
          tampil_data();
        }
      })

    });
  });
</script>