<!DOCTYPE html>
<html>
<head>
	<title>Pengembalian Buku</title>
</head>
<body>

	<div class="box">
		<h2>Data Pengembalian Buku</h2>
		<form action="<?php echo site_url('Pengembalian/add_process'); ?>" method="POST" id="id_kembali">
			<a href="<?=base_url()?>index.php">Back</a>
     
	<div class="table-responsive"> <br>
		<table border="1">
			<thead>
				<tr>
					<th>No</th>
					<th>Nama</th>
					<th>NIS</th>
					<th>Judul Buku</th>
					<th>Kode Buku</th>
					<th>Tgl Pinjam</th>
					<th>Tgl Pengembalian</th>
					<th>Option</th>
				</tr>
			</thead>
			<tbody>
		<?php $i=1; ?>
		<?php foreach ($pengembalian as $row ):?>
		<?php $i++ ?>
		<tr>
			<td><?=$i?></td>
			<td><?=$row["nama"]?></td>
			<td><?=$row["nis"]?></td>
			<td><?=$row["judul"]?></td>
			<td><?=$row["kode"]?></td>
			<td><?=$row["tgl_pinjam"]?></td>
			<td><?=$row["tgl_kembali"]?></td>
			<td>
			<a href="<?=base_url()?>index.php/pengembalian/edit/<?=$row["id_kembali"]?>">
				<button>Edit</button>
			</a>
		</td>
	</tr>
<?php endforeach;  ?>
	</tbody>

		</table>
		<button class="button"><?php echo anchor('Pengembalian/p_tambah','Input');?></button>
	</div>	
		
	</div>
</body>
</html>