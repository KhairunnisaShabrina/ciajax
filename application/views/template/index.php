﻿<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=Edge">
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <title>Welcome to Perpustakaan</title>
    
    <link rel="stylesheet" type="text/css"  href="<?php echo base_url('https://fonts.googleapis.com/css?family=Roboto:400,700&subset=latin,cyrillic-ext') ?>">
    <link  rel="stylesheet" type="text/css" href="<?php echo base_url('https://fonts.googleapis.com/icon?family=Material+Icons') ?>">

    <!-- Bootstrap Core Css -->
    <link rel="stylesheet" href="<?php echo base_url('Asset/plugins/bootstrap/css/bootstrap.css') ?>" />

    <!-- Waves Effect Css -->
    <link rel="stylesheet" href="<?php echo base_url('Asset/plugins/node-waves/waves.css') ?>" />

    <!-- Custom Css -->
    <link rel="stylesheet" href="<?php echo base_url('Asset/css/style.css') ?>">

    <!-- AdminBSB Themes. You can choose a theme from css/themes instead of get all themes -->
    <link rel="stylesheet" href="<?php echo base_url('Asset/css/themes/all-themes.css') ?>" />
</head>
<style type="">
    
</style>

<body class="theme-red">
    <!-- Page Loader -->
    <div class="page-loader-wrapper">
        <div class="loader">
            <div class="preloader">
                <div class="spinner-layer pl-red">
                    <div class="circle-clipper left">
                        <div class="circle"></div>
                    </div>
                    <div class="circle-clipper right">
                        <div class="circle"></div>
                    </div>
                </div>
            </div>
            
        </div>
    </div>
    <!-- #END# Page Loader -->
    <!-- Overlay For Sidebars -->
    <div class="overlay"></div>
    <!-- #END# Overlay For Sidebars -->
   
    <!-- Top Bar -->
    <nav class="navbar">
        <div class="container-fluid">
            <div class="navbar-header">
                <a href="javascript:void(0);" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-collapse" aria-expanded="false"></a>
                <a href="javascript:void(0);" class="bars"></a>
                <a class="navbar-brand" href="index.html"></a>
            </div>
            
                    <!-- Tasks -->
                    
                                </ul>
                            </li>
                        </ul>
                    </li>
                    <!-- #END# Tasks -->
                    
                </ul>
            </div>
        </div>
    </nav>
    <!-- #Top Bar -->
    <section>
        <!-- Left Sidebar -->
        <aside id="leftsidebar" class="sidebar">
            <!-- User Info -->
            <div class="user-info">
                <div class="image">
                    <img src="gambar/t.png" width="48" height="48" alt="User" /><p>ADMIN</p>
                </div>
                    </div>
                </div>
            </div>
            <!-- #User Info -->
            <!-- Menu -->
            <div class="menu">
                <ul class="list">
                   
                    <li class="active">
                        <a href="<?php echo site_url('user/tampil') ?>">
                            <i class="material-icons"></i>
                            <span>Anggota</span>
                        </a>
                    </li>
                    <li>
                        <a href="<?php echo site_url('buku_controllers/index') ?>">
                            <i class="material-icons"></i>
                            <span>Data Buku</span>
                        </a>
                    </li>
                    <li>
                        <a href="<?php echo site_url('home/index') ?>">
                            <i class="material-icons"></i>
                            <span>Peminjaman</span>
                        </a>
                    </li>
                    <li>
                        <a href="<?php echo site_url('Pengembalian/index') ?>">
                            <i class="material-icons"></i>
                            <span>Pengembalian</span>
                        </a>
                    </li>
                  
        </aside>
        <!-- #END# Left Sidebar -->
       
    <section class="content">
        <div class="container-fluid">
            <div class="block-header">
                <center><h2>Selamat Datang di Perpustakaan!</h2></center>
                <br>

            <!-- Widgets -->
                <div class="image2">
                    <img src="gambar/perpus.jpg" width="1020" height="470" alt="" />
                </div>
           
            <!-- #END# Widgets -->
            
            </div>

            </div>
        </div>
    </section>

    <!-- Jquery Core Js -->
    <script src="<?php echo base_url('Asset/plugins/jquery/jquery.min.js') ?>"></script>

    <!-- Bootstrap Core Js -->
    <script src="<?php echo base_url('Asset/plugins/bootstrap/js/bootstrap.js') ?>"></script>

    <!-- Select Plugin Js -->
    <script src="<?php echo base_url('Asset/plugins/bootstrap-select/js/bootstrap-select.js') ?>"></script>

    <!-- Slimscroll Plugin Js -->
    <script src="<?php echo base_url('Asset/plugins/jquery-slimscroll/jquery.slimscroll.js') ?>"></script>

    <!-- Waves Effect Plugin Js -->
    <script src="<?php echo base_url('Asset/plugins/node-waves/waves.js') ?>"></script>

    <!-- Jquery CountTo Plugin Js -->
    <script src="<?php echo base_url('Asset/plugins/jquery-countto/jquery.countTo.js') ?>"></script>

    <!-- Morris Plugin Js -->
    <script src="<?php echo base_url('Asset/plugins/raphael/raphael.min.js') ?>"></script>
    <script src="<?php echo base_url('Asset/plugins/morrisjs/morris.js') ?>"></script>

    <!-- Sparkline Chart Plugin Js -->
    <script src="<?php echo base_url('Asset/plugins/jquery-sparkline/jquery.sparkline.js') ?>"></script>

    <!-- Custom Js -->
    <script src="<?php echo base_url('Asset/js/admin.js') ?>"></script>
    <script src="<?php echo base_url('Asset/js/pages/index.js') ?>"></script>

    <!-- Demo Js -->
    <script src="<?php echo base_url('Asset/js/demo.js') ?>"></script>
</body>
</html>
