<!DOCTYPE html>
<html>
<head>
  <title>Data Anggota</title>
  <link rel="stylesheet" type="text/css" href="<?php echo base_url('mod/bootstrap/css/bootstrap.min.css'); ?>">
  <script type="text/javascript" src="<?php echo base_url('mod/jquery/jquery-3.3.1.min.js'); ?>"></script>
  <script type="text/javascript" src="<?php echo base_url('mod/bootstrap/js/bootstrap.min.js'); ?>"></script>

</head>
<body>
    <a href="<?=base_url()?>index.php">Back</a>

<center>    
 <body style="margin: 20px;">
  <div class="panel panel-primary">
    <div class="panel-heading">
      <b class="col-md-10">Data Anggota</b>
      <center><button data-toggle="modal" data-target="#addModal" class="btn btn-success">Tambah Data +</button></center>
    </div>
    <div class="panel-body">
      <div class="table-responsive">
        <table class="table table-striped table-bordered table-hover">
          <thead>
            <tr>
              <th>No</th>
              <th>ID</th>
              <th>Nama</th>
              <th>Jenis Kelamin</th>
              <th>NIS</th>
              <th>Kelas</th>
              <th>Alamat</th>
              <th>Option</th>
            </tr>
          </thead>
          <tbody id="tbl_data">
            
          </tbody>
        </table>
      </div>
    </div>
  </div>
</body>


        <!-- Modal Tambah-->
  <div id="addModal" class="modal fade" role="dialog">
    <div class="modal-dialog">

      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Tambah Data</h4>
        </div>
        <div class="modal-body">
          <form>
            <div class="form-group">
              <label for="nama" class="col-sm-1">Nama</label>
              <input type="text" name="nama" class="form-control"></input>
            </div>
            <div class="form-group">
              <label for="JK" class="col-sm-3">Jenis Kelamin</label>
              <select class='form-control' id='JK' name='JK'>
                    <option value="">--pilih--</option>
                    <option value="Laki-laki">Laki-laki</option>
                    <option value="Perempuan">Perempuan</option>
                </select>
            </div>
            <div class="form-group">
              <label for="nis" class="col-sm-1">NIS</label>
              <input type="text" name="nis" class="form-control"></input>
            </div>
            <div class='form-group'>
                <label class="col-sm-1">Kelas</label>
                <select class='form-control' id='kelas' name='kelas'>
                    <option value="">--pilih--</option>
                    <option value="X">X</option>
                    <option value="XI">XI</option>
                    <option value="XII">XII</option>
                </select>
            </div>
            <div class="form-group">
              <label for="alamat" class="col-sm-1">Alamat</label>
              <input type="text" name="alamat" class="form-control"></input>
            </div>

          </form>
        </div>
        <div class="modal-footer">
         <button type="button" class="btn btn-success" id="btn_add_data">Simpan</button>
         <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>

    </div>
  </div>

  <!-- Modal Edit-->
  <div id="editModal" class="modal fade" role="dialog">
    <div class="modal-dialog">

      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="submit" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Edit Data</h4>
        </div>
        <div class="modal-body">
          <form>
            <div class="form-group">
              <label for="id_user">ID</label>
              <input type="text" name="id_edit" class="form-control"></input>
            </div>
            <div class="form-group">
              <label for="nama">Nama</label>
              <input type="text" name="nama_edit" class="form-control"></input>
            </div>
             <div class="form-group">
              <label for="JK" class="col-sm-3">Jenis Kelamin</label>
              <select type='text' class='form-control' id='JK' name='JK_edit'>
                    <option value="">--pilih--</option>
                    <option value="Laki-laki">Laki-laki</option>
                    <option value="Perempuan">Perempuan</option>
                </select>
            </div>
            <div class="form-group">
              <label for="nis">NIS</label>
              <input type="text" name="nis_edit" class="form-control"></input>
            </div>
            <div class='form-group'>
                <label for="kelas">Kelas</label>
                <select type="text" name='kelas_edit' id='kelas' class='form-control'>
                    <option value="">--pilih--</option>
                    <option value="X">X</option>
                    <option value="XI">XI</option>
                    <option value="XII">XII</option>
                </select>
            </div>
            <div class="form-group">
              <label for="alamat">Alamat</label>
              <input type="text" name="alamat_edit" class="form-control"></input>
            </div>

          </form>
        </div>
        <div class="modal-footer">
         <button type="submit" class="btn btn-success" id="btn_update_data">Update</button>
         <button type="submit" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>

    </div>
  </div>

</html>
<script type="text/javascript">
  $(document).ready(function(){
    tampil_data();
    //Menampilkan Data di tabel
    function tampil_data(){
      $.ajax({
        url: '<?php echo site_url('User/ambilData'); ?>',
        type: 'POST',
        dataType: 'json',
        success: function(response){
          var i;
          var no = 0;
          var html = "";
          for(i=0;i < response.length ; i++){
            no++;
            html = html + '<tr>'
                  + '<td>' + no  + '</td>'
                  + '<td>' + response[i].id_user  + '</td>'
                  + '<td>' + response[i].nama  + '</td>'
                  + '<td>' + response[i].JK  + '</td>'
                  + '<td>' + response[i].nis  + '</td>'
                  + '<td>' + response[i].kelas  + '</td>'
                  + '<td>' + response[i].alamat + '</td>'
                  + '<td style="width: 16.66%;">' + '<span><button data-id="'+response[i].id_user+'" class="btn btn-primary btn_edit">Edit</button><button style="margin-left: 5px;" data-id="'+response[i].id_user+'" class="btn btn-danger btn_hapus">Hapus</button></span>'  + '</td>'
                  + '</tr>';
          }
          $("#tbl_data").html(html);
        }

      });
    }
    //Hapus Data dengan konfirmasi
    $("#tbl_data").on('click','.btn_hapus',function(){
      var id_user = $(this).attr('data-id');
      var status = confirm('Yakin ingin menghapus?');
      if(status){
        $.ajax({
          url: '<?php echo site_url('User/hapusData'); ?>',
          type: 'POST',
          data: {id_user:id_user},
          success: function(response){
            tampil_data();
          }
        })
      }
    })
    //Menambahkan Data ke database
    $("#btn_add_data").on('click',function(){
      var id_user = $('input[name="id_user"]').val();
      var nama = $('input[name="nama"]').val();
      var JK = $('select[name="JK"]').val();
      var nis = $('input[name="nis"]').val();
      var kelas = $('select[name="kelas"]').val();
      var alamat = $('input[name="alamat"]').val();
      $.ajax({
        url: '<?php echo site_url('User/tambahData'); ?>',
        type: 'POST',
        data: {nama:nama,JK:JK,id_user:id_user,nis:nis,kelas:kelas,alamat:alamat},
        success: function(response){
          $('input[name="id_user"]').val("");
          $('input[name="nama"]').val("");
          $('input[name="JK"]').val("");
          $('input[name="nis"]').val("");
          $('input[select="kelas"]').val("");
          $('input[name="alamat"]').val("");
          $("#addModal").modal('hide');
          tampil_data();
        }
      })

    });
    //Memunculkan modal edit
    $("#tbl_data").on('click','.btn_edit',function(){
      var id_user = $(this).attr('data-id');
      $.ajax({
        url: '<?php echo site_url('User/ambilDataByNoinduk'); ?>',
        type: 'POST',
        data: {id_user:id_user},
        dataType: 'json',
        success: function(response){
          console.log(response);
          $("#editModal").modal('show');
          $('input[name="id_edit"]').val(response[0].id_user);
          $('input[name="nama_edit"]').val(response[0].nama);
          $('input[select="JK_edit"]').val(response[0].JK);
          $('input[name="nis_edit"]').val(response[0].nis);
          $('input[select="kelas_edit"]').val(response[0].kelas);
          $('input[name="alamat_edit"]').val(response[0].alamat);

        }
      })
    });

    //Meng-Update Data
    $("#btn_update_data").on('click',function(){
      var id_user = $('input[name="id_edit"]').val();
      var nama = $('input[name="nama_edit"]').val();
      var JK  = $('select[name="JK_edit"]').val();
      var nis = $('input[name="nis_edit"]').val();
      var kelas = $('select[name="kelas_edit"]').val();
      var alamat = $('input[name="alamat_edit"]').val();
      $.ajax({
        url: '<?php echo site_url('User/perbaruiData'); ?>',
        type: 'POST',
        data: {nama:nama,JK:JK,id_user:id_user,nis:nis,kelas:kelas,alamat:alamat},
        success: function(response){
          $('input[name="id_edit"]').val("");
          $('input[name="nama_edit"]').val("");
          $('input[select="JK_edit"]').val("");
          $('input[name="nis_edit"]').val("");
          $('input[select="kelas_edit"]').val("");
          $('input[name="alamat_edit"]').val("");
          $("#editModal").modal('hide');
          tampil_data();
        }
      })

    });
  });
</script>