<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pengembalian extends CI_Controller {

	function __construct(){
		parent::__construct();
		$this->load->model('M_Pengembalian');
	}

	public function index()
	{
		$this->load->view('dashboard/pengembalian');
	}

	function ambilData()
	{
		$data = $this->M_Pengembalian->getData();
		echo json_encode($data);
	}

	function ambilDataByNoinduk(){
		$id_kembali = $this->input->post('id_kembali');
		$data = $this->M_Pengembalian->getDataByNoinduk($id_kembali);
		echo json_encode($data);
	}

	function hapusData(){
		$id_kembali = $this->input->post('id_kembali');
		$data = $this->M_Pengembalian->deleteData($id_kembali);
		echo json_encode($data);
	}

	function tambahData(){
		$id_kembali 	= $this->input->post('id_kembali');
		$nama 			= $this->input->post('nama');
		$nis 			= $this->input->post('nis');
		$kode 			= $this->input->post('kode');
		$judul 			= $this->input->post('judul');
		$tgl_pinjam 	= $this->input->post('tgl_pinjam');
		$tgl_kembali 	= $this->input->post('tgl_kembali');

		$data = ['id_kembali' => $id_kembali, 'nama' => $nama, 'nis' => $nis , 'kode' => $kode, 'judul' => $judul, 'tgl_pinjam' => $tgl_pinjam, 'tgl_kembali' => $tgl_kembali];
		$data = $this->M_Pengembalian->insertData($data);
		echo json_encode($data);
	}

	function perbaruiData(){
		$id_kembali 	= $this->input->post('id_kembali');
		$nama 			= $this->input->post('nama');
		$nis 			= $this->input->post('nis');
		$kode 			= $this->input->post('kode');
		$judul 			= $this->input->post('judul');
		$tgl_pinjam 	= $this->input->post('tgl_pinjam');
		$tgl_kembali 	= $this->input->post('tgl_kembali');

		$data = ['id_kembali' => $id_kembali, 'nama' => $nama, 'nis' => $nis , 'kode' => $kode, 'judul' => $judul, 'tgl_pinjam' => $tgl_pinjam, 'tgl_kembali' => $tgl_kembali];
		$data = $this->M_Pengembalian->updateData($id_kembali,$data);
		
		echo json_encode($data);
	}
}
