<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends CI_Controller {

	public function __construct(){
		parent::__construct();
		$this->load->model('m_user');
	}
	public function index(){
		$data["user"]=$this->m_user->tampil();
		$this->load->view('template/index',$data);
	}
	public function home2(){
		$this->load->view('dashboard/home2');
	}
}
