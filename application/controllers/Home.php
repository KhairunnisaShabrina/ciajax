<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {

	function __construct(){
		parent::__construct();
		$this->load->model('M_home');
	}

	public function index()
	{
		$data['buku_views']=$this->M_home->getAll();
		$this->load->view('dashboard/home_tampil');
	}

	function ambilData()
	{
		$data = $this->M_home->getData();
		echo json_encode($data);
	}

	function ambilDataByNoinduk(){
		$id_pinjam = $this->input->post('id_pinjam');
		$data = $this->M_home->getDataByNoinduk($id_pinjam);
		echo json_encode($data);
	}

	function hapusData(){
		$id_pinjam = $this->input->post('id_pinjam');
		$data = $this->M_home->deleteData($id_pinjam);
		echo json_encode($data);
	}

	function tambahData(){
		$id_pinjam 		= $this->input->post('id_pinjam');
		$nama 			= $this->input->post('nama');
		$nis 			= $this->input->post('nis');
		$judul 			= $this->input->post('judul');
		$kode 			= $this->input->post('kode');
		$tgl_pinjam 	= $this->input->post('tgl_pinjam');
		$status 		= $this->input->post('status');

		$data = ['id_pinjam' => $id_pinjam, 'nama' => $nama, 'nis' => $nis , 'judul' => $judul, 'kode' => $kode, 'tgl_pinjam' => $tgl_pinjam, 'status' => $status];
		$data = $this->M_home->insertData($data);
		echo json_encode($data);
	}

	function perbaruiData(){
		$id_pinjam 		= $this->input->post('id_pinjam');
		$nama 			= $this->input->post('nama');
		$nis 			= $this->input->post('nis');
		$judul 			= $this->input->post('judul');
		$kode 			= $this->input->post('kode');
		$tgl_pinjam 	= $this->input->post('tgl_pinjam');
		$status 		= $this->input->post('status');

		$data = ['id_pinjam' => $id_pinjam, 'nama' => $nama, 'nis' => $nis , 'judul' => $judul, 'kode' => $kode, 'tgl_pinjam' => $tgl_pinjam, 'status' => $status];
		$data = $this->M_home->updateData($id_pinjam,$data);
		
		echo json_encode($data);
	}
}
