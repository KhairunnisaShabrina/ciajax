<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Buku_controllers extends CI_Controller {

	function __construct(){
		parent::__construct();
		$this->load->model('M_Buku');
	}

	public function index(){
		$this->load->view('dashboard/buku_views');
	}

	function ambilData()
	{
		$data = $this->M_Buku->getData();
		echo json_encode($data);
	}

	function ambilDataByNoinduk(){
		$id_buku = $this->input->post('id_buku');
		$data = $this->M_Buku->getDataByNoinduk($id_buku);
		echo json_encode($data);
	}

	function hapusData(){
		$id_buku = $this->input->post('id_buku');
		$data = $this->M_Buku->deleteData($id_buku);
		echo json_encode($data);
	}

	function tambahData(){
		$id_buku 		= $this->input->post('id_buku');
		$nama_buku 		= $this->input->post('nama_buku');
		$penerbit 		= $this->input->post('penerbit');
		$pengarang 		= $this->input->post('pengarang');
		$jumlah 		= $this->input->post('jumlah');
		$jenis  		= $this->input->post('jenis');

		$data = ['id_buku' => $id_buku, 'nama_buku' => $nama_buku, 'penerbit' => $penerbit, 'pengarang' => $pengarang , 'jumlah' => $jumlah, 'jenis' => $jenis];
		$data = $this->M_Buku->insertData($data);
		echo json_encode($data);
	}

	function perbaruiData(){
		$id_buku 		= $this->input->post('id_buku');
		$nama_buku 		= $this->input->post('nama_buku');
		$penerbit 		= $this->input->post('penerbit');
		$pengarang 		= $this->input->post('pengarang');
		$jumlah 		= $this->input->post('jumlah');
		$jenis 		    = $this->input->post('jenis');

		$data = ['id_buku' => $id_buku, 'nama_buku' => $nama_buku, 'penerbit' => $penerbit, 'pengarang' => $pengarang , 'jumlah' => $jumlah, 'jenis' => $jenis];
		$data = $this->M_Buku->updateData($id_buku, $data);
		echo json_encode($data);
	}
}







