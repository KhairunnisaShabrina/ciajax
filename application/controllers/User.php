<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User extends CI_Controller {

	function __construct(){
		parent::__construct();
		$this->load->model('M_user');
	}

	public function index(){
		$this->load->view('template/index');
	}

	public function tampil(){
		$this->load->view('template/userr');
	}

	function ambilData()
	{
		$data = $this->M_user->getData();
		echo json_encode($data);
	}

	function ambilDataByNoinduk(){
		$id_user = $this->input->post('id_user');
		$data = $this->M_user->getDataByNoinduk($id_user);
		echo json_encode($data);
	}

	function hapusData(){
		$id_user = $this->input->post('id_user');
		$data = $this->M_user->deleteData($id_user);
		echo json_encode($data);
	}

	function tambahData(){
		$id_user 		= $this->input->post('id_user');
		$nama 			= $this->input->post('nama');
		$JK 			= $this->input->post('JK');
		$nis 			= $this->input->post('nis');
		$kelas 			= $this->input->post('kelas');
		$alamat 		= $this->input->post('alamat');

		$data = ['id_user' => $id_user, 'nama' => $nama, 'JK' => $JK, 'nis' => $nis , 'kelas' => $kelas, 'alamat' => $alamat];
		$data = $this->M_user->insertData($data);
		echo json_encode($data);
	}

	function perbaruiData(){
		$id_user 		= $this->input->post('id_user');
		$nama 			= $this->input->post('nama');
		$JK 			= $this->input->post('JK');
		$nis 			= $this->input->post('nis');
		$kelas 			= $this->input->post('kelas');
		$alamat 		= $this->input->post('alamat');

		$data = ['id_user' => $id_user, 'nama' => $nama, 'JK' => $JK, 'nis' => $nis , 'kelas' => $kelas, 'alamat' => $alamat];
		$data = $this->M_user->updateData($id_user, $data);
		echo json_encode($data);
	}
}







