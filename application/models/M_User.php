<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_User extends CI_Model 
{

	function tampil(){
		return $this->db->get('user');
	}
	function getData(){
		return $this->db->get('user')->result();
	}

	function getDataByNoinduk($id_user){
		$this->db->where('id_user',$id_user);
		return $this->db->get('user')->result();
	}

	function deleteData($id_user){
		$this->db->where('id_user',$id_user);
		$this->db->delete('user');
	}

	function insertData($data){
		$this->db->insert('user',$data);
	}

	function updateData($id_user,$data){
		$this->db->where('id_user',$id_user);
		$this->db->update('user',$data);
	}
}
?>
