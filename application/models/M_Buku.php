<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_Buku extends CI_Model 
{

	function tampil(){
		return $this->db->get('buku');
	}
	function getData(){
		return $this->db->get('buku')->result();
	}

	function getDataByNoinduk($id_buku){
		$this->db->where('id_buku',$id_buku);
		return $this->db->get('buku')->result();
	}

	function deleteData($id_buku){
		$this->db->where('id_buku',$id_buku);
		$this->db->delete('buku');
	}

	function insertData($data){
		$this->db->insert('buku',$data);
	}

	function updateData($id_buku,$data){
		$this->db->where('id_buku',$id_buku);
		$this->db->update('buku',$data);
	}
}
?>
