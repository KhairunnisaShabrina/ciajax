<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_Pengembalian extends CI_Model 
{
	
	function getData(){
		return $this->db->get('pengembalian_a')->result();
	}

	function getDataByNoinduk($id_kembali){
		$this->db->where('id_kembali',$id_kembali);
		return $this->db->get('pengembalian_a')->result();
	}

	function deleteData($id_kembali){
		$this->db->where('id_kembali',$id_kembali);
		$this->db->delete('pengembalian_a');
	}

	function insertData($data){
		$this->db->insert('pengembalian_a',$data);
	}

	function updateData($id_kembali,$data){
		$this->db->where('id_kembali',$id_kembali);
		$this->db->update('pengembalian_a',$data);
	}

}
?>









