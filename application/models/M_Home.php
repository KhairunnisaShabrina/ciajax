<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_Home extends CI_Model 
{
	function getData(){
		$this->db->select('home_tampil.*, buku_views.nama_buku as id_tampil');
		$this->db->from('home_tampil');
		$this->db->join('buku_views','buku_views.id_buku = home_tampil.id_tampil', 'left');
		$query = $this->db->get();
		return $query->result();
	}

	function getDataByNoinduk($id_pinjam){
		$this->db->where('id_pinjam',$id_pinjam);
		return $this->db->get('pinjam_b')->result();
	}

	function deleteData($id_pinjam){
		$this->db->where('id_pinjam',$id_pinjam);
		$this->db->delete('pinjam_b');
	}

	function insertData($data){
		$this->db->insert('pinjam_b',$data);
	}

	function updateData($id_pinjam,$data){
		$this->db->where('id_pinjam',$id_pinjam);
		$this->db->update('pinjam_b',$data);
	}

	function getAll(){
		return $this->db->get('buku_views')->result();
	}

}
?>









